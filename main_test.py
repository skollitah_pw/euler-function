import unittest

import main


class TestEulerFunction(unittest.TestCase):

    def test_euler_function(self):
        self.assertEqual(main.phi(3458), 1296)
        self.assertEqual(main.phi(234), 72)
        self.assertEqual(main.phi(25), 20)
        self.assertEqual(main.phi(567), 324)


if __name__ == '__main__':
    unittest.main()
