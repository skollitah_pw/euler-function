def gcd(a, b):
    if a == b:
        return a
    if a == 0:
        return b
    if b == 0:
        return a

    result = 1
    count = 0

    while a != b:
        if b > a:
            tmp = a
            a = b
            b = tmp

        x = ((a & 1) << 1) | (b & 1)

        if x == 0:
            result <<= 1
            a >>= 1
            b >>= 1
        elif x == 1:
            a >>= 1
        elif x == 2:
            b >>= 1
        elif x == 3:
            a = (abs(a - b) >> 1)

        count += 1

    return result * a


def phi(n):
    amount = 0
    for k in range(1, n + 1):
        if gcd(n, k) == 1:
            amount += 1
    return amount


if __name__ == '__main__':
    try:
        n = int(input('n: '))
        print(f'Euler function for n={n} is equal: {phi(n)}')
    except Exception as e:
        raise e
